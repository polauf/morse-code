#[macro_use]
extern crate lazy_static;

pub mod morse;
use std::io;

fn main() {
    let mut text;
    let mut input_line = String::new();
    println!();
    println!("Welcome to MORSE CODE!");
    println!();
    loop {
        input_line.clear();
        println!("Please write a text for translation and then press [enter]:");
        io::stdin().read_line(&mut input_line).unwrap();
        text = input_line.trim();
        if text.len() > 0 {
            let encoded =  morse::Morse::encode(text).collect::<String>();
            let encoded_sep = morse::Morse::encode(text).map(|s| "/".to_string() + s ).collect::<String>();
            println!("Encoded for streaming  : {}", encoded);
            println!("With separators        : {}", encoded_sep);
            println!("Decoded back           : {}", morse::Morse::decode(&encoded_sep, "/").trim());
            println!("If you want to transmit the message wirelessly to the nearest receivers, then write '>' and press [ENTER] otherwise press [ENTER].");
            input_line.clear();
            io::stdin().read_line(&mut input_line).unwrap();
            if input_line.trim() == ">"{ morse::Morse::play_boop(&encoded_sep); }
        }
    }
}
