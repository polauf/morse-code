use std::{collections::HashMap};
use core::{str::Chars, iter::FilterMap};

use soloud::*;

lazy_static! {
  pub static ref MORSE_ALPHABET: HashMap<char, &'static str> = {
    HashMap::from([
      ('a', ".-"),
      ('b', "-..."),
      ('c', "-.-."),
      ('d', "-.."),
      ('e', "."),
      ('f', "..-."),
      ('g', "--."),
      ('h', "...."),
      ('i', ".."),
      ('j', ".---"),
      ('k', "-.-"),
      ('l', ".-.."),
      ('m', "--"),
      ('n', "-."),
      ('o', "---"),
      ('p', ".--."),
      ('q', "--.-"),
      ('r', ".-."),
      ('s', "..."),
      ('t', "-"),
      ('u', "..-"),
      ('v', "...-"),
      ('w', ".--"),
      ('x', "-..-"),
      ('y', "-.--"),
      ('z', "--.."),
      ('1', ".----"),
      ('2', "..---"),
      ('3', "...--"),
      ('4', "....-"),
      ('5', "....."),
      ('6', "-...."),
      ('7', "--..."),
      ('8', "---.."),
      ('9', "----."),
      ('0', "-----"),
      (',', "--..--"),
      ('?', "..--.."),
      (':', "---..."),
      ('&', ".-..."),
      ('\'', ".----."),
      ('@', ".--.-."),
      (')', "-.--.-"),
      ('(', "-.--."),
      ('=', "-...-"),
      ('!', "-.-.--"),
      ('.', ".-.-.-"), // Full Stop
      ('-', "-....-"),
      ('%', "------..-.-----"),
      ('+', ".-.-."),
      ('"', ".-..-."),
    ])
  };
  pub static ref ABBRAV: HashMap<String, &'static str> = {
    HashMap::from([
      ("AA".to_string(), ".-.-"),           // New Line
      ("AR".to_string(), ".-.-."),          // End of message 
      ("AS".to_string(), ".-..."),          // Wait
      ("BK".to_string(), "-...-.-"),        // Break
      ("BT".to_string(), "-...-"),          // New Paragraph
      ("CL".to_string(), "-.-..-.."),       // Going off the air ("clear") 	
      ("CT".to_string(), "-.-.-"),          // Start copying
      ("DO".to_string(), "-..---"),         // Change to wabun code
      ("KA".to_string(), "-.-.-"),          // Starting signal
      ("KN".to_string(), "-.--."),          // Invite a specific station to transmit
      ("SK".to_string(), "...-.-"),         // End of transmission 
      ("SN".to_string(), "...-."),          // Understood
      ("SOS".to_string(), "...---..."),     // Distress
      ("ERROR".to_string(), "........"),    // Error

      ("73".to_string(), "--......--"),     // Best Regasrds
      ("88".to_string(), "---..---.."),     // Love and kisses
      ("BCNU".to_string(), "-...-.-.-...-"),     // Be seeing you
      ("CQ".to_string(), "-.-.--.-"),       // Call to all stations
      ("CS".to_string(), "-.-...."),        // Call sign (request)
      ("CUL".to_string(), "-.-...-.-.."),   // See you later
      ("DE".to_string(), "-..."),           // From (or "this is")
      ("ES".to_string(), "...."),           // And
      ("K".to_string(), "-.-"),             // Over (invitation to transmit)
      ("OM".to_string(), "-----"),          // Old man
      ("R".to_string(), ".-."),             // Received / Roger
      ("RST".to_string(), ".-....-"),       // Signal report
      ("UR".to_string(), "..-.-."),         // You Are

      ("QRL".to_string(), "--.-.-..-.."),   // The frequency is in use
      ("QRM".to_string(), "--.-.-.--"),     // Your transmission is being interfered with (1-5)
      ("QRN".to_string(), "--.-.-.-."),     // I am troubled by static (1-5)
      ("QRO".to_string(), "--.-.-.---"),    // Increase transmitter power
      ("QRP".to_string(), "--.-.-..--."),   // Decrease transmitter power
      ("QRQ".to_string(), "--.-.-.--.-"),   // Send faster (...words per minute)
      ("QRS".to_string(), "--.-.-...."),    // Send more slowly (...words per minute)
      ("QRT".to_string(), "--.-.-.-"),      // Stop sending
      ("QRU".to_string(), "--.-.-...-"),    // I have nothing for you
      ("QRV".to_string(), "--.-.-....-"),   // I am ready to copy
      ("QRX".to_string(), "--.-.-.-..-"),   // Wait
      ("QRZ".to_string(), "--.-.-.--.."),   // You are being called by...
      ("QSB".to_string(), "--.-...-..."),   // Your signals are fading
      ("QSL".to_string(), "--.-....-.."),   // I acknowledge receipt
      ("QTH".to_string(), "--.--...."),     // My location is...

      ("QRL?".to_string(), "--.-.-..-....--.."),   // The frequency is in use
      ("QRM?".to_string(), "--.-.-.--..--.."),     // Your transmission is being interfered with (1-5)
      ("QRN?".to_string(), "--.-.-.-...--.."),     // I am troubled by static (1-5)
      ("QRO?".to_string(), "--.-.-.---..--.."),    // Increase transmitter power
      ("QRP?".to_string(), "--.-.-..--...--.."),   // Decrease transmitter power
      ("QRQ?".to_string(), "--.-.-.--.-..--.."),   // Send faster (...words per minute)
      ("QRS?".to_string(), "--.-.-......--.."),    // Send more slowly (...words per minute)
      ("QRT?".to_string(), "--.-.-.-..--.."),      // Stop sending
      ("QRU?".to_string(), "--.-.-...-..--.."),    // I have nothing for you
      ("QRV?".to_string(), "--.-.-....-..--.."),   // I am ready to copy
      ("QRX?".to_string(), "--.-.-.-..-..--.."),   // Wait
      ("QRZ?".to_string(), "--.-.-.--....--.."),   // You are being called by...
      ("QSB?".to_string(), "--.-...-.....--.."),   // Your signals are fading
      ("QSL?".to_string(), "--.-....-....--.."),   // I acknowledge receipt
      ("QTH?".to_string(), "--.--......--.."),     // My location is...
    ])
  };
}

pub struct Morse;

impl Morse {
  pub fn encode<'a>(text: &'a str) -> FilterMap<Chars<'a>, impl FnMut(char) -> Option<&'a str>>{
    let mut multi = String::new();
    let mut multi_enabled = false;
    text.chars().filter_map(move |c| {
      let letter = c.to_ascii_lowercase();
      if letter == '<' {
        multi_enabled = true;
        None
      } else if multi_enabled && letter != '>' {
        
        multi += &letter.to_string();
        None
      } else if letter == '>' {
        multi_enabled = false;
        let abb = *ABBRAV.get(&multi.to_uppercase()).unwrap_or(&"*");
        multi.clear();
        Some(abb)
      } else {
        let code = *MORSE_ALPHABET.get(&letter).unwrap_or(&"*");
        Some(code)
      }
    })
  }

  pub fn decode(code: &str, separator: &str) -> String {
    code.to_string().split(separator).filter_map( |l| {
      match MORSE_ALPHABET
        .iter()
        .find(|(_, &c)| c==l) {
          Some(a) => Some(a.0.to_string()),
          None => match ABBRAV
            .iter()
            .find(|(_, &c)| c==l) {
              Some(a) => Some(format!("<{}>",a.0)),
              None => None
            }
        }      
    }).collect::<String>().to_string()
  }

  pub fn play_boop(code: &str) {
    let sl = Soloud::default().expect("Soloud: Loading Error");
    let mut speech = audio::Speech::default();
    let beep_boop: String = code.chars().map(|m| {
        let b = if m =='.' { 
          "2 "
        } else if m == '/' { 
          ";;;;. "
        } else if m == '*' { 
          "er "
        } else {
          "beeb "
        };
        b
    }).collect();
    speech.set_text(&beep_boop).expect("Soloud: Speech loading Error");

    sl.play(&speech);
    while sl.active_voice_count() > 0 {
        std::thread::sleep(std::time::Duration::from_millis(100));
    }    
  }
}
